#[macro_use]
extern crate serde_derive;

macro_rules! builder {
    ($struct_name:ident {
        $($attr_name:ident: $attr_type:ty),* $(,)*
    }) => {
        #[derive(Default, Serialize)]
        #[serde(rename_all="camelCase")]
        pub struct $struct_name {
            $(
                #[serde(skip_serializing_if = "Option::is_none")]
                $attr_name: Option<$attr_type>,
            )*
        }

        impl $struct_name {
            $(
                pub fn $attr_name(mut self, $attr_name: $attr_type) -> Self {
                    self.$attr_name = Some($attr_name);
                    self
                }
            )*
        }
    };
}

// TODO implement override in datasets for mixed chart support
/// Built-in char types.
///
/// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/)
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub enum Type {
    // TODO: implement configurations
    /// A line chart is a way of plotting data points on a line. Often, it is used to show trend
    /// data, or the comparison of two data sets.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""line""#, serde_json::to_string(&Type::Line).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/line.html)
    Line,

    /// A bar chart provides a way of showing data values represented as vertical bars. It is
    /// sometimes used to show trend data, and the comparison of multiple data sets side by side.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""bar""#, serde_json::to_string(&Type::Bar).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/bar.html)
    Bar,

    /// A radar chart is a way of showing multiple data points and the variation between them.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""radar""#, serde_json::to_string(&Type::Radar).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/radar.html)
    Radar,

    /// Pie and doughnut charts are probably the most commonly used charts. They are divided into
    /// segments, the arc of each segment shows the proportional value of each piece of data.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""pie""#, serde_json::to_string(&Type::DoughnutAndPie).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/doughnut.html)
    #[serde(rename = "pie")]
    DoughnutAndPie,

    /// Polar area charts are similar to pie charts, but each segment has the same angle - the
    /// radius of the segment differs depending on the value.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""polarArea""#, serde_json::to_string(&Type::PolarArea).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/polar.html)
    PolarArea,

    /// A bubble chart is used to display three dimensions of data at the same time. The location of
    /// the bubble is determined by the first two dimensions and the corresponding horizontal and
    /// vertical axes. The third dimension is represented by the size of the individual bubbles.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""bubble""#, serde_json::to_string(&Type::Bubble).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/bubble.html)
    Bubble,

    /// Scatter charts are based on basic line charts with the x axis changed to a linear axis. To
    /// use a scatter chart, data must be passed as objects containing X and Y properties.
    ///
    /// ```
    /// # use chartjs::Type;
    /// assert_eq!(r#""scatter""#, serde_json::to_string(&Type::Scatter).unwrap());
    /// ```
    ///
    /// [Chart.js Documentation](https://www.chartjs.org/docs/latest/charts/scatter.html)
    Scatter,
}
#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum Position {
    Top,
    Left,
    Bottom,
    Right,
}

#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum FontStyle {
    Normal,
    Bold,
    // TODO: other styles
}

#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum TooltipMode {
    Point,
    Nearest,
    Index,
    XAxis,
    DataSet,
    X,
    Y,
}

#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum TooltipPosition {
    Average,
    Nearest,
}

#[derive(Serialize)]
#[serde(rename_all = "snake_case")]
pub enum PointStyle {
    Circle,
    Cross,
    CrossRot,
    Dash,
    Line,
    Rect,
    RectRounded,
    RectRot,
    Star,
    Triangle,
}

pub trait Data {
    // TODO define properties
}

builder!(Padding {
    left: u16,
    right: u16,
    top: u16,
    bottom: u16,
});

builder!(Layout { padding: Padding });

type Color = String;

builder!(Font {
    size: u16,
    family: String, // TODO: consider enum
    style: FontStyle,
    color: Color,
});

builder!(LegendLabels {
    box_width: u16,
    font: Font,
    padding: u16,
    // TODO: generateLabels,
    // TODO: filter,
    use_point_style: bool,
});

builder!(Legend {
    display: bool,
    position: Position,
    full_width: bool,
    // TODO: onClick,
    // TODO: onHover,
    revers: bool,
    labels: LegendLabels,
});

builder!(Title {
    display: bool,
    position: Position,
    font: Font,
    padding: u16,
    line_height: f32,
    text: String,
});

builder!(Tooltip {
    enabled: bool,
    // TODO: custom,
    mode: TooltipMode,
    intersect: bool,
    position: TooltipPosition,
    // TODO: callback,
    // TODO: itemSort,
    // TODO: filter,
    background_color: Color,
    title_font: Font,
    title_spacing: u16,
    title_margin_bottom: u16,
    body_font: Font,
    body_spacing: u16,
    footer_font: Font,
    footer_spacing: u16,
    footer_margin_top: u16,
    x_padding: u16,
    y_padding: u16,
    caret_padding: u16,
    caret_size: u16,
    corner_radius: u16,
    multi_key_background: Color,
    display_colors: bool,
    border_color: Color,
    border_width: u16,
});

builder!(Elements {
    radius: u16,
    point_style: PointStyle,
    rotation: u16, // TODO: consider limiting to just valid degree range
    background_color: Color,
    border_width: u16,
    border_color: Color,
    hit_radius: u16,
    hover_radius: u16,
    hover_border_width: u16,
});

builder!(Options {
    // TODO: animations
    layout: Layout,
    legend: Legend,
    title: Title,
    tooltip: Tooltip,
    elements: Elements,
});

pub struct Chart<'a> {
    r#type: Type,
    data: &'a Data,
    options: Options,
}

impl<'a> Chart<'a> {
    pub fn new(t: Type, data: &'a Data, options: Option<Options>) -> Self {
        Self {
            r#type: t,
            data,
            options: options.unwrap_or(Options::default()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! assert_serializes_to {
        ($left:expr, $right:expr) => {
            assert_eq!(serde_json::to_string(&$left).unwrap(), $right);
        };
    }

    macro_rules! assert_serializes_to_empty {
        ($sut:expr) => {
            assert_eq!(serde_json::to_string(&$sut).unwrap(), "{}");
        };
    }

    #[test]
    fn fully_configured_padding() {
        assert_serializes_to!(
            Padding::default().left(1).right(2).top(3).bottom(4),
            r#"{"left":1,"right":2,"top":3,"bottom":4}"#
        );
    }

    #[test]
    fn not_configured_padding() {
        assert_serializes_to_empty!(Padding::default());
    }

    #[test]
    fn partially_configured_padding() {
        assert_serializes_to!(Padding::default().top(2), r#"{"top":2}"#);
    }

    #[test]
    fn configured_layout() {
        assert_serializes_to!(
            Layout::default().padding(Padding::default()),
            r#"{"padding":{}}"#
        );
    }

    #[test]
    fn not_configured_layout() {
        assert_serializes_to_empty!(Layout::default());
    }

    #[test]
    fn fully_configured_font() {
        assert_serializes_to!(
            Font::default()
                .size(12)
                .family("Helvatica".into())
                .style(FontStyle::Normal)
                .color("#fff".into()),
            r##"{"size":12,"family":"Helvatica","style":"normal","color":"#fff"}"##
        );
    }

    #[test]
    fn not_configured_font() {
        assert_serializes_to_empty!(Font::default());
    }

}
